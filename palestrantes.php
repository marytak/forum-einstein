<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Palestrantes | 1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/palestrantes.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <ul class="mn_einstein">
                        <li><a href="#" target="_blank">Hospital</a></li>
                        <li><a href="#" target="_blank">Medicina Diagnostica</a></li>
                        <li><a href="#" target="_blank">Ensino</a></li>
                        <li><a href="#" target="_blank">Pesquisa</a></li>
                        <li><a href="#" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="#" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="#" class="port">Português</a></li>
                        <li><a href="#" class="eng">English</a></li>
                        <li><a href="#" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="logo"><a href="index.php">1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</a></div>
                <ul class="mn_azul">
                    <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> como chegar</a></li>
                    <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> hospedagem</a></li>
                    <li><a href="contato.php"><i class="fa fa-envelope"></i> contato</a></li>
                    <li><a href="duvidas.php"><i class="fa fa-question-circle"></i> dúvidas frequentes</a></li>
                </ul>
                <hr>
                <ul class="mn_princ">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="evento.php">evento</a></li>
                    <li><a href="programacao.php">programação</a></li>
                    <li><a href="palestrantes.php" class="select">palestrantes</a></li>
                    <li><a href="inscricoes.php">inscrições</a></li>
                    <li><a href="patrocinadores.php">patrocinadores</a></li>
                </ul>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>palestrantes</h1>
                    <div class="org">
                        <p>Organização</p>
                        <ul>
                            <li><img src="images/lgo_einstein_top.png" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="98" height="44"></li>
                            <li><img src="images/lgo_institute.png" alt="Institute for Healthcare Improvement" title="Institute for Healthcare Improvement" width="107" height="48"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central palestrantes">
                <div class="col_left">
                    <div class="holder">
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/donald_berwick.jpg" alt="Donald M. Berwick" title="Donald M. Berwick" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Donald M. Berwick</li>
                                <li class="cargo">Presidente Emérito e Senior Fellow</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/dan_heath.jpg" alt="Dan Heath" title="Dan Heath" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Dan Heath</li>
                                <li class="cargo">Senior Fellow</li>
                                <li class="empresa">Duke University’s CASE Center</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/maureen_bisognano.jpg" alt="Maureen Bisognano" title="Maureen Bisognano" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Maureen Bisognano</li>
                                <li class="cargo">Presidente e CEO</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/gary_cohen.jpg" alt="Gary Cohen" title="Gary Cohen" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Gary Cohen</li>
                                <li class="cargo">Diretor Executivo</li>
                                <li class="empresa">Health Care Without Harm (HCWH)</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/ademir_petenate.jpg" alt="Ademir José Petenate" title="Ademir José Petenate" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Ademir José Petenate</li>
                                <li class="cargo">Unicamp</li>
                                <li class="empresa">Departamento de Estatística</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/claudio_lottenberg.jpg" alt="Claudio Lottenberg" title="Claudio Lottenberg" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Claudio Lottenberg</li>
                                <li class="cargo">Presidente</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/frank_federico.jpg" alt="Frank Federico" title="Frank Federico" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Frank Federico</li>
                                <li class="cargo">RPh, Diretor Executivo e Strategic Partners</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/gary_kaplan.jpg" alt="Gary S. Kaplan" title="Gary S. Kaplan" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Gary S. Kaplan</li>
                                <li class="cargo">Presidente e CEO</li>
                                <li class="empresa">Sistema de Saúde Virginia Mason</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/henrique_neves.jpg" alt="Henrique Neves" title="Henrique Neves" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Henrique Neves</li>
                                <li class="cargo">Diretor geral</li>
                                <li class="empresa">Sociedade Beneficente Israelita Brasileira Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/jeffrey_simmons.jpg" alt="Jeffrey Simmons" title="Jeffrey Simmons" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Jeffrey Simmons</li>
                                <li class="empresa"> Cincinnati Children’s Hospital Medical Center</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/joseph_crane.jpg" alt="Joseph T. Crane" title="Joseph T. Crane" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Joseph T. Crane</li>
                                <li class="cargo">Medicina de Emergência</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/lisa_schiling.jpg" alt="Lisa Schilling" title="Lisa Schilling" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Lisa Schilling</li>
                                <li class="cargo">Vice-Presidente Nacional</li>
                                <li class="empresa">Healthcare Performance Improvement</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/marcos_tucherman.jpg" alt="Marcos Tucherman" title="Marcos Tucherman" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Marcos Tucherman</li>
                                <li class="cargo">Gerente de Sustentabilidade</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/miguel_neto.jpg" alt="Miguel Cendoroglo Neto" title="Miguel Cendoroglo Neto" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Miguel Cendoroglo Neto</li>
                                <li class="cargo">Diretor Médico</li>
                                <li class="empresa">Hospital Israelita Albert Einstein</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/paulo_borem.jpg" alt="Paulo Borem" title="Paulo Borem" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Paulo Borem</li>
                                <li class="cargo">Coordenador de Qualidade e Segurança</li>
                                <li class="empresa">Paciente no Sistema Unimed Brasil</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/pedro_delgado.jpg" alt="Pedro Delgado" title="Pedro Delgado" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Pedro Delgado</li>
                                <li class="cargo">Diretor executivo</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/shahab_saeed.jpg" alt="Shahab Saeed" title="Shahab Saeed" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Shahab Saeed</li>
                                <li class="cargo">Vice Presidente e COO</li>
                                <li class="empresa">Questar Energy Services</li>
                            </ul>
                        </div>
                        <div class="item">
                            <a href="#">
                                <img src="images/palestrantes/somava_stout.jpg" alt="Somava S. Stout" title="Somava S. Stout" width="178" height="184">
                                <span class="hover">+<br>MINI CV</span>
                            </a>
                            <ul>
                                <li class="nome">Somava S. Stout</li>
                                <li class="cargo"> Líder Executiva Externa para Melhoria na Saúde</li>
                                <li class="empresa">IHI</li>
                            </ul>
                        </div>
                    </div>
                    <p>Levando em conta a política do IHI, os professores participantes deste congresso deverão informar no início de suas apresentações qualquer interesse econômico ou pessoal que crie ou que possa ser entendido como criador de conflitos relacionados ao material em discussão. O objetivo de tal informação não é o de evitar a apresentação de um palestrante que possua qualquer interesse financeiro significativo, mas sim o de informar aos ouvintes para que estes façam seu próprio julgamento. A não ser quando notificado acima, cada palestrante forneceu todas as informações e não pretende discutir o uso não aprovado/experimental de um produto ou dispositivo comercial e não possui nenhum interesse financeiro significativo para realizar qualquer promoção. Caso usos não aprovados de produtos venham a ser discutidos, os palestrantes deverão informar o fato aos participantes.</p>
                </div>
                <div class="col_right">
                    <div class="top">
                        <i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Praesent ac sem in neque venenatis tristique. Morbi et ligula velit. Nullam a augue vel mi porta vestibulum non ac elit. Vivamus convallis tortor et fermentum semper.</p>
                    <a href="#" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="#" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="#" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery-cycle.js"></script>
        <script src="js/palestrantes.js"></script>

    </body>
</html>