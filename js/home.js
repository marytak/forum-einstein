$( document ).ready(function() {
    $("#bkg_banner").cycle({ 
        fx:     'fade'
    });
    $("#banner .central").cycle({ 
        fx:     'fade'
    });
    $(".galeria li")
    .stop().mouseenter(function(){
        $(this).find("span").stop().fadeIn();
    }).stop().mouseleave(function(){
        $(this).find("span").stop().fadeOut();
    });
});