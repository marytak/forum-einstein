$(document).ready(function () {
    //Abas
    $("#tabs > ul.abas > li > a").click(function(evt) {
        evt.preventDefault();
        var clicked = $(this);
        clicked.parent().parent().find("li").removeClass("selected");
        clicked.parent().addClass("selected");
        $("#tabs > .content_aba").hide();
        $(clicked.attr('href')).show();
    });
});