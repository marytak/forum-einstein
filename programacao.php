<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Programação | 1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/programacao.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <ul class="mn_einstein">
                        <li><a href="#" target="_blank">Hospital</a></li>
                        <li><a href="#" target="_blank">Medicina Diagnostica</a></li>
                        <li><a href="#" target="_blank">Ensino</a></li>
                        <li><a href="#" target="_blank">Pesquisa</a></li>
                        <li><a href="#" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="#" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="#" class="port">Português</a></li>
                        <li><a href="#" class="eng">English</a></li>
                        <li><a href="#" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="logo"><a href="index.php">1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</a></div>
                <ul class="mn_azul">
                    <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> como chegar</a></li>
                    <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> hospedagem</a></li>
                    <li><a href="contato.php"><i class="fa fa-envelope"></i> contato</a></li>
                    <li><a href="duvidas.php"><i class="fa fa-question-circle"></i> dúvidas frequentes</a></li>
                </ul>
                <hr>
                <ul class="mn_princ">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="evento.php">evento</a></li>
                    <li><a href="programacao.php" class="select">programação</a></li>
                    <li><a href="palestrantes.php">palestrantes</a></li>
                    <li><a href="inscricoes.php">inscrições</a></li>
                    <li><a href="patrocinadores.php">patrocinadores</a></li>
                </ul>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>programação</h1>
                    <div class="org">
                        <p>Organização</p>
                        <ul>
                            <li><img src="images/lgo_einstein_top.png" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="98" height="44"></li>
                            <li><img src="images/lgo_institute.png" alt="Institute for Healthcare Improvement" title="Institute for Healthcare Improvement" width="107" height="48"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central programacao">
                <div class="col_left">
                    
                    <div id="tabs">
                        <ul class="abas">
                            <li class="dia1 selected"><a href="#dia1"><span>DIA 1<br>13 de agosto de 2015</span></a></li>
                            <li class="dia2"><a href="#dia2"><span>DIA 2<br>14 de agosto de 2015</span></a></li>
                            <li class="dia3"><a href="#dia3"><span>DIA 3<br>15 de agosto de 2015</span></a></li>
                            <li class="dia4"><a href="#dia4"><span>DIA 4<br>16 de agosto de 2015</span></a></li>
                        </ul>
                        <div id="dia1" class="content_aba">
                            <img src="images/seta01.png" class="seta">
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 9h30 - 10h30</p>
                                <p class="tit">Keynote 1 – “Flipping Healthcare”<br>(Atendimento focado na pessoa e na família)</p>
                                <hr>
                                <p class="palestrante"><i class="fa fa-microphone"></i> Maureen Bisognano</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 11h - 11h30</p>
                                <p class="tit">Intervalo</p>
                            </div>
                            <div class="item">
                                <p class="hora"><i class="fa fa-clock-o"></i> 11h30 - 12h45</p>
                                <p class="tit">Workshops Simlutâneos</p>
                                <p class="subtit">Sessão A – Cinco opções de sessões independentes</p>
                                <ul>
                                    <li>
                                        <span class="subtit">1. Habilidades da liderança para transformações - Capacidade de melhoria</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> Maureen Bisognano | Palestrante 2 | Palestrante 3</span>
                                    </li>
                                    <li>
                                        <span class="subtit">2. Organização para alta confiabilidade - Segurança do paciente</span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> Maureen Bisognano | Palestrante 2 | Palestrante 3</span>
                                    </li>
                                    <li>
                                        <span class="subtit">3. Acelerando as transformações na saúde com inovação e abordagem enxuta - Estudo de caso </span>
                                        <hr>
                                        <span class="palestrante"><i class="fa fa-microphone"></i> Maureen Bisognano | Palestrante 2 | Palestrante 3</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="dia2" class="content_aba" style="display:none">
                            <img src="images/seta02.png" class="seta">
                            dia2
                        </div>
                        <div id="dia3" class="content_aba" style="display:none">
                            <img src="images/seta03.png" class="seta">
                            dia3
                        </div>
                        <div id="dia4" class="content_aba" style="display:none">
                            <img src="images/seta04.png" class="seta">
                            dia4
                        </div>
                    </div>
                    
                </div>
                <div class="col_right">
                    <div class="top">
                        <i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Praesent ac sem in neque venenatis tristique. Morbi et ligula velit. Nullam a augue vel mi porta vestibulum non ac elit. Vivamus convallis tortor et fermentum semper.</p>
                    <a href="#" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="#" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="#" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery-cycle.js"></script>
        <script src="js/programacao.js"></script>

    </body>
</html>