<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        
        <title>Hospedagem | 1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</title>

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="stylesheet" href="css/swag.css" />
        <link rel="stylesheet" href="css/internas.css" />
        <link rel="stylesheet" href="css/orfao.css" />
        
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    </head>
    <body>

        <header>
            <div id="header_hosp">
                <div class="central">
                    <ul class="mn_einstein">
                        <li><a href="#" target="_blank">Hospital</a></li>
                        <li><a href="#" target="_blank">Medicina Diagnostica</a></li>
                        <li><a href="#" target="_blank">Ensino</a></li>
                        <li><a href="#" target="_blank">Pesquisa</a></li>
                        <li><a href="#" target="_blank">Responsabilidade Social</a></li>
                        <li><a href="#" target="_blank">Einstein Saúde</a></li>
                    </ul>
                    <ul class="idioma">
                        <li><a href="#" class="port">Português</a></li>
                        <li><a href="#" class="eng">English</a></li>
                        <li><a href="#" class="esp">Español</a></li>
                    </ul>
                    <div class="social">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="menu" class="central border">
                <div class="logo"><a href="index.php">1º Fórum Latino Americano sobre Qualidade e Segurança na Saúde - Em busca de sustentabilidade</a></div>
                <ul class="mn_azul">
                    <li><a href="localizacao.php"><i class="fa fa-map-marker"></i> como chegar</a></li>
                    <li><a href="hospedagem.php"><i class="fa fa-suitcase"></i> hospedagem</a></li>
                    <li><a href="contato.php"><i class="fa fa-envelope"></i> contato</a></li>
                    <li><a href="duvidas.php"><i class="fa fa-question-circle"></i> dúvidas frequentes</a></li>
                </ul>
                <hr>
                <ul class="mn_princ">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="evento.php">evento</a></li>
                    <li><a href="programacao.php">programação</a></li>
                    <li><a href="palestrantes.php">palestrantes</a></li>
                    <li><a href="inscricoes.php">inscrições</a></li>
                    <li><a href="patrocinadores.php">patrocinadores</a></li>
                </ul>
            </div>
            <div class="tit">
                <div class="central">
                    <h1>hospedagem</h1>
                    <div class="org">
                        <p>Organização</p>
                        <ul>
                            <li><img src="images/lgo_einstein_top.png" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="98" height="44"></li>
                            <li><img src="images/lgo_institute.png" alt="Institute for Healthcare Improvement" title="Institute for Healthcare Improvement" width="107" height="48"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <div id="content" class="internas">
            
            <div class="central">
                <div class="col_left">
                    <p>Aqui você encontrará algumas informações que poderão contribuir para sua melhor acomodação, transporte e bem-estar durante a estadia em São Paulo.</p>

                    <h2>AD Ag. de Viagens e Turismo</h2>
                    <p>
                        <span class="bold">Contato</span>: Carla Klajner<br>
                        <span class="bold">E-mail</span>: ihi@adturismo.com.br <br>
                        <span class="bold">Endereço</span>: Rua Estela, 164 - São Paulo, SP<br>
                        <span class="bold">Tel.</span>: (55 11) 5087-3455<br>
                        <span class="bold">Fax.</span>: (55 11) 5087-3455
                    </p>

                    <h2>HOSPEDAGEM</h2>
                    <p>Abaixo uma relação de hotéis localizados nas proximidades do evento. </p>
                    <p>Para mais informações sobre disponibilidade e tarifas diferenciadas aos participantes, entre em contato com a agência de turismo oficial.</p>
                    <p>
                        <span class="bold">Sheraton São Paulo WTC</span><br>
                        <span class="bold">Endereço</span>: Av. Das Nações Unidas 12.559 - Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: no próprio local do evento<br>
                        <a href="http://www.sheratonsaopaulowtc.com" target="_blank">www.sheratonsaopaulowtc.com</a>
                    </p>
                    <p>
                        <span class="bold">Gran Estanplaza São Paulo</span><br>
                        <span class="bold">Endereço</span>: Rua Arizona, 1.517 - Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: em frente ao Sheraton São Paulo WTC<br>
                        <a href="http://www.estanplaza.com.br/gran-estanplaza/institucional" target="_blank">www.estanplaza.com.br/gran-estanplaza/institucional</a>
                    </p>
                    <p>
                        <span class="bold">Grand Hyatt São Paulo</span><br>
                        <span class="bold">Endereço</span>: Av. Das Nações Unidas, 13.301 – Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: 1 km<br>
                        <a href="http://saopaulo.grand.hyatt.com" target="_blank">http://saopaulo.grand.hyatt.com</a>
                    </p>
                    <p>
                        <span class="bold">Hilton São Paulo Morumbi</span><br>
                        <span class="bold">Endereço</span>: Av. Das Nações Unidas, 12.901 – Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: dentro do complexo WTC<br>
                        <a href="http://www3.hilton.com" target="_blank">www3.hilton.com</a>
                    </p>
                    <p>  
                        <span class="bold">Transamérica Executive</span><br>
                        <span class="bold">Endereço</span>: Rua Américo Brasiliense, 2.163<br>
                        <span class="bold">Bairro</span>: Chácara Santo Antônio<br>
                        <span class="bold">Distância aproximada do evento</span>: 3 km<br>
                        <a href="http://www.transamericagroup.com.br/br/nossos-hoteis/executive/chacara" target="_blank">www.transamericagroup.com.br/br/nossos-hoteis/executive/chacara</a>
                    </p>
                    <p>
                        <span class="bold">Transamérica Executive Congonhas</span><br>
                        <span class="bold">Endereço</span>: Rua Vieira de Morais, 1960 – Campo Belo<br>
                        <span class="bold">Distância aproximada do evento</span>: 7 km<br>
                        <a href="http://www.transamericagroup.com.br/br/nossos-hoteis/executive/congonhas" target="_blank">www.transamericagroup.com.br/br/nossos-hoteis/executive/congonhas</a>
                    </p>
                    <p>
                        <span class="bold">Blue Tree Morumbi </span><br>
                        <span class="bold">ENdereço</span>: Av. Roque Petroni Junior, 1000 – Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: 2,5 km<br>
                        <a href="http://www.bluetree.com.br/hotel/blue-tree-premium-morumbi" target="_blank">www.bluetree.com.br/hotel/blue-tree-premium-morumbi</a>
                    </p>
                    <p>
                        <span class="bold">Blue Tree Premium Verbo Divino</span><br>
                        <span class="bold">Endereço</span>: Rua Verbo Divino, 1.323 – Chácara Santo Antonio<br>
                        <span class="bold">Distância aproximada do evento</span>: 3,5 km<br>
                        <a href="http://www.bluetree.com.br/hotel/blue-tree-premium-verbo-divino" target="_blank">www.bluetree.com.br/hotel/blue-tree-premium-verbo-divino</a>
                    </p>
                    <p>
                        <span class="bold">Estanplaza Berrini</span><br>
                        <span class="bold">Endereço</span>: Av. Engenheiro Luis Carlos Berrini , 853 – Brooklin Novo<br>
                        <span class="bold">Distância aproximada do evento</span>: 900 m<br>
                        <a href="http://www.estanplaza.com.br/hoteis-boutique/estanplaza-berrini/resumo" target="_blank">www.estanplaza.com.br/hoteis-boutique/estanplaza-berrini/resumo</a>
                    </p>
                    <p>
                        <span class="bold">Tryp Nações Unidas</span><br>
                        <span class="bold">Endereço</span>: Rua Fernandes Moreira , 1264 – Chácara Santo Antonio<br>
                        <span class="bold">Distância aproximada do evento</span>: 3 km<br>
                        <a href="http://www.melia.com/pt/hoteis/brasil/sao-paulo/tryp-sao-paulo-nacoes-unidas-hotel/index.html" target="_blank">www.melia.com/pt/hoteis/brasil/sao-paulo/tryp-sao-paulo-nacoes-unidas-hotel/index.html</a>
                    </p>
                    <p>
                        <span class="bold">Mercure São Paulo Nações Unidas</span><br>
                        <span class="bold">Endereço</span>: Rua Prof. Manoelito de Ornelas , 104 – Chácara Santo Antonio<br>
                        <span class="bold">Distância aproximada do evento</span>: 3,5 km<br>
                        <a href="http://www.mercure.com/pt-br/hotel-3135-mercure-sao-paulo-nacoes-unidas-hotel/index.shtml" target="_blank">www.mercure.com/pt-br/hotel-3135-mercure-sao-paulo-nacoes-unidas-hotel/index.shtml</a>
                    </p>
                    <p>
                        <span class="bold">Intercity Premium Nações Unidas</span><br>
                        <span class="bold">Endereço</span>: Rua Fernandes Moreira, 1371 – Chácara Santo Antonio<br>
                        <span class="bold">Distância aproximada do evento</span>: 3 km<br>
                        <a href="http://www.intercityhoteis.com.br/hoteis/intercity-premium-nacoes-unidas" target="_blank">www.intercityhoteis.com.br/hoteis/intercity-premium-nacoes-unidas</a>
                    </p>

                    <h2>LAZER E TURISMO</h2>
                    <p>Para desfrutar melhor de sua estadia em São Paulo, indicamos abaixo alguns pontos turísticos que podem complementar sua experiência.</p>

                    <p>
                        <strong>COMPRAS</strong><br>
                        D&D Shopping<br>
                        Shopping Cidade Jardim<br>
                        Shopping Iguatemi<br>
                        Shopping Morumbi<br>
                        Vila Madalena<br>
                        Oscar Freire
                    </p>
                    <p>
                        <strong>ESPORTE</strong><br>
                        Estádio Cícero Pompeu de Toledo (Morumbi)<br>
                        Estádio Municipal Paulo Machado de Carvalho (Pacaembu)<br>
                        Jockey Club de São Paulo<br>
                        Autódromo de Interlagos
                    </p>
                    <p>
                        <strong>MUSEUS</strong><br>
                        MASP<br>
                        Museu do Futebol<br>
                        Museu do Ipiranga<br>
                        Pinacoteca do Estado<br>
                        Museu da Língua Portuguesa
                    </p>
                    <p>
                        <strong>MONUMENTOS</strong><br>
                        Ponte Estaiada Octávio Frias de Oliveira
                    </p>
                    <p>
                        <strong>BARES E RESTAURANTES</strong><br>
                        Sky<br>
                        Barbacoa<br>
                        Churrascaria Fogo de Chão<br>
                        Cervejaria Devassa
                    </p>
                    <p>
                        <strong>LAZER E ENTRETENIMENTO</strong><br>
                        Sala São Paulo<br>
                        Mercado Municipal<br>
                        Parque do Ibirapuera<br>
                        Avenida Paulista<br>
                        Liberdade
                    </p>

                </div>
                <div class="col_right">
                    <div class="top">
                        <i class="fa fa-plus-circle"></i> <i class="fa fa-calendar"></i> <p><strong>Data</strong><br>13 a 16 de agosto de 2015</p>
                    </div>
                    <p class="txt">Praesent ac sem in neque venenatis tristique. Morbi et ligula velit. Nullam a augue vel mi porta vestibulum non ac elit. Vivamus convallis tortor et fermentum semper.</p>
                    <a href="#" class="inscrevase border">inscreva-se <i class="fa fa-arrow-circle-right"></i></a>
                    <div class="novidades">
                        <h3><i class="fa fa-newspaper-o"></i>receba novidades</h3>
                        <form>
                            <fieldset>
                                <input type="text" placeholder="Nome" class="border">
                                <input type="email" placeholder="E-mail" class="border">
                                <input type="text" placeholder="Cargo" class="border">
                                <input type="text" placeholder="Empresa" class="border">
                                <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                            </fieldset>
                        </form>
                    </div>
                    <div class="compartilhe">
                        <p>Compartilhe:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="central">
                <div class="localizacao">
                    <h3><i class="fa fa-map-marker"></i> localização</h3>
                    <div class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.867364509829!2d-46.696393699999966!3d-23.609089299999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50cce72e241b%3A0xc7a372d415cdbf2e!2sAv.+das+Na%C3%A7%C3%B5es+Unidas%2C+12559+-+Itaim+Bibi%2C+S%C3%A3o+Paulo+-+SP%2C+04795-100!5e0!3m2!1sen!2sbr!4v1417469205101" width="364" height="266" frameborder="0"></iframe>
                    </div>
                </div>
                <div class="novidades">
                    <h3><i class="fa fa-newspaper-o"></i> receba novidades</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <input type="text" placeholder="Cargo" class="border">
                            <input type="text" placeholder="Empresa" class="border">
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
                <div class="contato">
                    <h3><i class="fa fa-envelope"></i> contato</h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="Nome" class="border">
                            <input type="email" placeholder="E-mail" class="border">
                            <textarea class="border" placeholder="Mensagem"></textarea>
                            <button class="border">enviar <i class="fa fa-arrow-circle-right"></i></button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </footer>
        <div id="footer_hosp">
            <div class="central">
                <a href="#" target="_blank"><img src="images/lgo_einstein_footer.png" class="aehi" alt="Albert Einstein Hospital Israelita" title="Albert Einstein Hospital Israelita" width="74" height="34"></a>
                <p>Copyright Albert Einstein 2015 | Todos os direitos reservados</p>
                <a href="#" target="_blank"><img src="images/lgo_mantra.png" class="mantra" alt="Agência Mantra" title="Agência Mantra" width="55" height="13"></a>
            </div>
        </div>

        <script src="js/plugin/jquery.min.js"></script>
        <script src="js/plugin/jquery-cycle.js"></script>

    </body>
</html>